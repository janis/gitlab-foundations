import { defineConfig } from 'vite';
import customElementsManifest from 'vite-plugin-cem';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  build: {
    sourcemap: true,
    outDir: 'dist',
  },
  plugins: [
    vue({customElement: true}),
    customElementsManifest({
      lit: true,
      files: [
        './src/main.ts'
      ]
    }),
  ],
})
