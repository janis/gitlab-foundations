# ⚡GitLab Foundations

This is a proof of concept GitLab component library that leverages custom element
support introduced in Vue 3.2. It uses the SCSS variables and mixins from
[`gitlab-org/gitlab-ui`](https://gitlab.com/gitlab-org/gitlab-ui), but styles
are inlined into each component rather than shared globally.

At time of writing only `gitlab-button` and `gitlab-icon` components are
implemented. The resulting production bundle is just under `22KB` gzipped
including all the inlined CSS.

## Development

- `npm run dev`: starts a development server with hot reloading
- `npm run build`: generates a production bundle at `dist/`
- `npm run preview`: serves the static asssets from `dist/`
