import { defineCustomElement } from 'vue';
import Button from './components/Button.ce.vue';
import Icon from './components/Icon.ce.vue';

const GitLabButton = defineCustomElement(Button);
const GitLabIcon = defineCustomElement(Icon);

export { GitLabButton, GitLabIcon };

export function register(prefix: string = 'gitlab') {
  customElements.define(`${prefix}-button`, GitLabButton);
  customElements.define(`${prefix}-icon`, GitLabIcon);
}
